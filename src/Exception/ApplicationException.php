<?php

namespace App\Exception;

use \Exception;

/**
 * Application exception for error handling
 *
 * Class ApplicationException
 *
 * @package App\Exception
 */
class ApplicationException extends Exception {

    /**
     * Redefine the exception so message isn't optional
     *
     * ApplicationException constructor.
     *
     * @param $message
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct($message, $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}