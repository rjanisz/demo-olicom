<?php

namespace App\Model;

/**
 * Class ResponseError
 *
 * @package App\Model
 */
class ResponseError extends Response {

    protected $message;

    /**
     * ResponseError constructor.
     *
     * @param $message
     */
    public function __construct($message) {
        parent::__construct(Response::STATUS_ERROR);
        $this->message = $message;
    }

    /**
     * Get message
     *
     * @return String
     */
    public function getMessage() {
        return $this->message;
    }
}