<?php

namespace App\Model;

/**
 * Class Response
 *
 * @package App\Model
 */
abstract class Response extends Model {

    const STATUS_SUCCESS = 'success';
    const STATUS_ERROR = 'error';

    protected $status;

    /**
     * Response constructor.
     *
     * @param $status
     */
    public function __construct($status) {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return String
     */
    public function getStatus() {
        return $this->status;
    }
}