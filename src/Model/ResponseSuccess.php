<?php

namespace App\Model;

/**
 * Class ResponseSuccess
 *
 * @package App\Model
 */
class ResponseSuccess extends Response {

    protected $data;

    /**
     * ResponseSuccess constructor.
     *
     * @param array $data
     */
    public function __construct(array $data) {
        parent::__construct(Response::STATUS_SUCCESS);
        $this->data = $data;
    }

    /**
     * Get data
     * 
     * @return array
     */
    public function getData() {
        return $this->data;
    }
}