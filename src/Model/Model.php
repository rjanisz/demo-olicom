<?php

namespace App\Model;

/**
 * Class Model
 *
 * @package App\Model
 */
abstract class Model {

    /**
     * Get object as array
     *
     * @return array
     */
    public function asArray() {
        return get_object_vars($this);
    }
}