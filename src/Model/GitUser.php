<?php

namespace App\Model;

/**
 * Class GitUser
 *
 * @package App\Model
 */
class GitUser extends Model {

    protected $name;
    protected $email;
    protected $url;
    protected $createdAt;

    /**
     * GitUser constructor.
     *
     * @param array $data
     */
    public function __construct(array $data) {
        $this->name = $data['name'];
        $this->email = $data['email'];
        $this->url = $data['url'];
        $this->createdAt = date_format(date_create($data['created_at']), DATE_ISO8601);
    }

    /**
     * Get name
     *
     * @return String
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Get name
     *
     * @return String
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Get URL
     *
     * @return String
     */
    public function getUrl() {
        return $this->url;
    }

    /**
     * Get date created
     *
     * @return false|String
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }
}