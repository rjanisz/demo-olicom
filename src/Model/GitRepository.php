<?php

namespace App\Model;

/**
 * Class GitRepository
 *
 * @package App\Model
 */
class GitRepository extends Model {

    protected $fullName;
    protected $description;
    protected $cloneUrl;
    protected $stars;
    protected $createdAt;

    /**
     * GitRepository constructor.
     *
     * @param array $data
     */
    public function __construct(array $data) {
        $this->fullName = $data['full_name'];
        $this->description = $data['description'];
        $this->cloneUrl = $data['clone_url'];
        $this->stars = $data['stargazers_count'];
        $this->createdAt = date_format(date_create($data['created_at']), DATE_ISO8601);
    }

    /**
     * Get full name
     *
     * @return String
     */
    public function getFullName() {
        return $this->fullName;
    }

    /**
     * Get description
     *
     * @return String
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Get clone URL
     *
     * @return String
     */
    public function getCloneUrl() {
        return $this->cloneUrl;
    }

    /**
     * Get stars
     *
     * @return false|String
     */
    public function getStars() {
        return $this->stars;
    }

    /**
     * Get date created
     *
     * @return false|String
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }
}