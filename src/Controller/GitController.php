<?php

namespace App\Controller;

use App\Exception\ApplicationException;
use App\Model\ResponseError;
use App\Model\ResponseSuccess;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Service\GitService;

/**
 * @Route("/git", name="git_")
 */
class GitController extends AbstractController {

    /**
     * Endpoint that give info of user data
     *
     * @Route("/users/{username}", name="user")
     *
     * @param GitService $gitService
     * @param String $username
     *
     * @return JsonResponse
     */
    public function user(GitService $gitService, String $username) {
        try {
            $gitUser = $gitService->getUser($username);
            $response = new ResponseSuccess(['user' => $gitUser->asArray()]);

            return new JsonResponse($response->asArray());
        } catch (ApplicationException $exception) {
            $response = new ResponseError($exception->getMessage());

            return new JsonResponse($response->asArray(), $exception->getCode());
        }
    }

    /**
     * Endpoint that give info of user repository data
     *
     * @Route("/repositories/{username}/{repository}", name="repository")
     *
     * @param GitService $gitService
     * @param String $username
     * @param String $repository
     *
     * @return JsonResponse
     */
    public function repository(GitService $gitService, String $username, String $repository) {
        try {
            $gitRepository = $gitService->getRepository($username, $repository);
            $response = new ResponseSuccess(['repository' => $gitRepository->asArray()]);

            return new JsonResponse($response->asArray());
        } catch (ApplicationException $exception) {
            $response = new ResponseError($exception->getMessage());

            return new JsonResponse($response->asArray(), $exception->getCode());
        }
    }

    /**
     * Endpoint that give info of users data. Usernames have to divided by `,` character
     *
     * @Route("/users/multi/{usernames}", name="users")
     *
     * @param GitService $gitService
     * @param String $usernames
     *
     * @return JsonResponse
     */
    public function users(GitService $gitService, String $usernames) {
        $names = explode(',', $usernames);

        try {
            $gitUsers = $gitService->getUsers($names);
        } catch (ApplicationException $exception) {
            $response = new ResponseError($exception->getMessage());

            return new JsonResponse($response->asArray(), $exception->getCode());
        }

        $users = [];
        foreach ($gitUsers as $gitUser) {
            $users[] = $gitUser->asArray();
        }

        $response = new ResponseSuccess(['users' => $users]);

        return new JsonResponse($response->asArray());
    }

    /**
     * Endpoint that give info of repositories data. Usernames and repositories have to divided by `,` character
     *
     * @Route("/repositories/multi/{usernames}/{repositories}", name="repositories")
     *
     * @param GitService $gitService
     * @param String $usernames
     * @param String $repositories
     *
     * @return JsonResponse
     *
     * @throws \Throwable
     */
    public function repositories(GitService $gitService, String $usernames, String $repositories) {
        $names = explode(',', $usernames);
        $repos = explode(',', $repositories);

        $data = [];
        foreach ($names as $key => $name) {
            $data[] = [
                'username' => $name,
                'repository' => $repos[$key]
            ];
        }

        try {
            $gitRepositories = $gitService->getRepositories($data);
        } catch (ApplicationException $exception) {
            $response = new ResponseError($exception->getMessage());

            return new JsonResponse($response->asArray(), $exception->getCode());
        }

        $repositories = [];
        foreach ($gitRepositories as $gitRepository) {
            $repositories[] = $gitRepository->asArray();
        }

        $response = new ResponseSuccess(['repositories' => $repositories]);

        return new JsonResponse($response->asArray());
    }
}