<?php

namespace App\Service;

use App\Exception\ApplicationException;
use App\Model\GitRepository;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Model\GitUser;
use GuzzleHttp\Psr7\Response;

/**
 * Class GitService
 *
 * @package App\Service
 */
class GitService {

    const GIT_BASE_URL = 'https://api.github.com/';

    private $appGitAccessToken;
    private $requestService;

    /**
     * GitService constructor.
     *
     * @param ParameterBagInterface $params
     */
    public function __construct(ParameterBagInterface $params, RequestService $requestService) {
        $this->appGitAccessToken = $params->get('app_git_access_token');
        $this->requestService = $requestService;
    }

    /**
     * Get GIT user by username
     *
     * @param String $name
     *
     * @return GitUser
     *
     * @throws ApplicationException
     */
    public function getUser(String $name) {
        $response = $this->requestService->fetch(GitService::GIT_BASE_URL . 'users/' . $name, [
            'Authorization' => 'token ' . $this->appGitAccessToken,
            'Content-Type' => 'application/json'
        ]);

        return new GitUser(json_decode($response->getBody(), true));
    }

    /**
     * Get GIT user repository by user username and repository name
     *
     * @param String $username
     * @param String $name
     *
     * @return GitRepository
     *
     * @throws ApplicationException
     */
    public function getRepository(String $username, String $name) {
        $response = $this->requestService->fetch(GitService::GIT_BASE_URL . 'users/' . $username . '/repos', [
            'Authorization' => 'token ' . $this->appGitAccessToken,
            'Content-Type' => 'application/json'
        ]);

        $repository = null;

        $repos = json_decode($response->getBody(), true);
        foreach ($repos as $repo) {
            if ($repo['name'] == $name) {
                $repository = $repo;
            }
        }

        if (is_null($repository)) {
            throw new ApplicationException('Invalid response status code.', 404);
        }

        return new GitRepository($repository);
    }

    /**
     * Get GIT users by usernames
     *
     * @param array $names
     *
     * @return GitUser []
     *
     * @throws ApplicationException
     */
    public function getUsers(array $names) {
        $requests = [];
        foreach ($names as $name) {
            $requests[] = [
                'url' => GitService::GIT_BASE_URL . 'users/' . $name,
                'headers' => [
                    'Authorization' => 'token ' . $this->appGitAccessToken,
                    'Content-Type' => 'application/json'
                ]
            ];
        }

        $responses = $this->requestService->fetchAsync($requests);

        $users = [];
        foreach ($responses as $response) {
            if ($response instanceof Response) {
                if ($response->getStatusCode() != 200) {
                    throw new ApplicationException('Invalid response status code.', $response->getStatusCode());
                }

                $users[] = new GitUser(json_decode($response->getBody(), true));
            } else {
                /** @var \GuzzleHttp\Psr7\Response $responseBody */
                $responseBody = $response['value'];
                $users[] = new GitUser(json_decode($responseBody->getBody(), true));
            }
        }

        return $users;
    }

    /**
     * Get GIT users repos
     *
     * @param array $data
     *
     * @return array
     *
     * @throws ApplicationException
     * @throws \Throwable
     */
    public function getRepositories(array $data) {
        $requests = [];
        foreach ($data as $user) {
            $requests[] = [
                'url' => GitService::GIT_BASE_URL . 'users/' . $user['username'] . '/repos',
                'headers' => [
                    'Authorization' => 'token ' . $this->appGitAccessToken,
                    'Content-Type' => 'application/json'
                ]
            ];
        }

        $responses = $this->requestService->fetchAsync($requests);

        $repositories = [];
        foreach ($responses as $key => $response) {
            if ($response instanceof Response) {
                if ($response->getStatusCode() != 200) {
                    throw new ApplicationException('Invalid response status code.', $response->getStatusCode());
                }

                $repository = null;

                $repos = json_decode($response->getBody(), true);
                foreach ($repos as $repo) {
                    if ($repo['name'] == $data[$key]['repository']) {
                        $repository = $repo;
                    }
                }

                if (is_null($repository)) {
                    throw new ApplicationException('Invalid response status code.', 404);
                }

                $repositories[] = new GitRepository($repository);
            } else {
                /** @var \GuzzleHttp\Psr7\Response $responseBody */
                $responseBody = $response['value'];

                $repository = null;

                $repos = json_decode($responseBody->getBody(), true);
                foreach ($repos as $repo) {
                    if ($repo['name'] == $data[$key]['repository']) {
                        $repository = $repo;
                    }
                }

                if (is_null($repository)) {
                    throw new ApplicationException('Invalid response status code.', 404);
                }

                $repositories[] = new GitRepository($repository);
            }
        }

        return $repositories;
    }
}