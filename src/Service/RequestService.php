<?php

namespace App\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use App\Exception\ApplicationException;
use GuzzleHttp\Promise;

/**
 * Service for fetching data from external services
 *
 * Class RequestService
 *
 * @package App\Service
 */
class RequestService {

    private $client;

    /**
     * RequestService constructor.
     */
    public function __construct() {
        $this->client = new Client(['http_errors' => false]);
    }

    /**
     * Method for fetching data with guzzle library
     *
     * @param String $url
     * @param array $headers
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     *
     * @throws ApplicationException
     */
    public function fetch(String $url, array $headers) {
        try {
            $response = $this->client->request('GET', $url, ['headers' => $headers]);
            if ($response->getStatusCode() != 200) {
                throw new ApplicationException('Invalid response status code.', $response->getStatusCode());
            }

            return $response;
        } catch (GuzzleException $exception) {
            throw new ApplicationException('Request error: ' . $exception->getMessage());
        }
    }

    /**
     * Method for fetching async requests
     *
     * @param array $data
     *
     * @return array
     *
     * @throws \Throwable
     */
    public function fetchAsync(array $data) {
        $promises = [];
        foreach ($data as $key => $request) {
            $promises[$key] = $this->client->requestAsync('GET', $request['url'], $request['headers']);
        }

        return Promise\unwrap($promises);
    }
}