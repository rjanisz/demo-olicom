<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class GitControllerTest
 *
 * @package App\Tests\Controller
 */
class GitControllerTest extends WebTestCase {

    /**
     * Test single user call to Github API with success response
     */
    public function testSingleUserFetch() {
        $client = static::createClient();
        $client->request('GET', '/git/users/zonk');

        $statusCode = $client->getResponse()->getStatusCode();
        $content = $client->getResponse()->getContent();

        $json = json_decode($content, true);

        $this->assertEquals(200, $statusCode);
        $this->assertEquals(true, strlen($content) > 0);
        $this->assertEquals(true, count($json) == 2);
        $this->assertEquals(true, array_key_exists('status', $json));
        $this->assertEquals('success', $json['status']);

        $data = $json['data'];
        $this->assertEquals(true, count($data) == 1);
        $this->assertEquals(true, array_key_exists('user', $data));

        $user = $data['user'];
        $this->assertEquals(true, count($user) == 4);

        $this->assertEquals(true, array_key_exists('name', $user));
        $this->assertEquals('Sebastian', $user['name']);

        $this->assertEquals(true, array_key_exists('email', $user));
        $this->assertEquals(null, $user['email']);

        $this->assertEquals(true, array_key_exists('url', $user));
        $this->assertEquals('https://api.github.com/users/zonk', $user['url']);

        $this->assertEquals(true, array_key_exists('createdAt', $user));
        $this->assertEquals('2012-03-15T11:13:43+0000', $user['createdAt']);
    }

    /**
     * Test single user call to Github API with error response
     */
    public function testSingleUserFetchError() {
        $client = static::createClient();
        $client->request('GET', '/git/users/zonk111');

        $statusCode = $client->getResponse()->getStatusCode();
        $content = $client->getResponse()->getContent();

        $json = json_decode($content, true);

        $this->assertEquals(404, $statusCode);

        $this->assertEquals(true, strlen($content) > 0);
        $this->assertEquals(true, count($json) == 2);

        $this->assertEquals(true, array_key_exists('status', $json));
        $this->assertEquals('error', $json['status']);

        $message = $json['message'];
        $this->assertEquals(true, is_string($message));
        $this->assertEquals('Invalid response status code.', $message);
    }

    /**
     * Test multiple user call to Github API with success responses
     */
    public function test20UserFetch() {
        $client = static::createClient();

        $multiply = 2;

        $names = str_repeat("zonk,", $multiply);
        $names = substr($names, 0, strlen($names) - 1);

        $startTime = microtime(true);
        $client->request('GET', '/git/users/multi/' . $names);
        $elapsedTime = microtime(true) - $startTime;

        $this->assertEquals(true, $elapsedTime <= 1.0);
    }

    /**
     * Test single repository call to Github API with success response
     */
    public function testSingleRepositoryFetch() {
        $client = static::createClient();
        $client->request('GET', '/git/repositories/octocat/git-consortium');

        $statusCode = $client->getResponse()->getStatusCode();
        $content = $client->getResponse()->getContent();

        $json = json_decode($content, true);

        $this->assertEquals(200, $statusCode);
        $this->assertEquals(true, strlen($content) > 0);
        $this->assertEquals(true, count($json) == 2);
        $this->assertEquals(true, array_key_exists('status', $json));
        $this->assertEquals('success', $json['status']);

        $data = $json['data'];
        $this->assertEquals(true, count($data) == 1);
        $this->assertEquals(true, array_key_exists('repository', $data));

        $repository = $data['repository'];
        $this->assertEquals(true, count($repository) == 5);

        $this->assertEquals(true, array_key_exists('fullName', $repository));
        $this->assertEquals('octocat/git-consortium', $repository['fullName']);

        $this->assertEquals(true, array_key_exists('description', $repository));
        $this->assertEquals('This repo is for demonstration purposes only.', $repository['description']);

        $this->assertEquals(true, array_key_exists('cloneUrl', $repository));
        $this->assertEquals('https://github.com/octocat/git-consortium.git', $repository['cloneUrl']);

        $this->assertEquals(true, array_key_exists('stars', $repository));
        $this->assertEquals(17, $repository['stars']);

        $this->assertEquals(true, array_key_exists('createdAt', $repository));
        $this->assertEquals('2014-03-28T17:55:38+0000', $repository['createdAt']);
    }

    /**
     * Test single repository call to Github API with error response
     */
    public function testSingleRepositoryFetchError() {
        $client = static::createClient();
        $client->request('GET', '/git/repositories/octocat/git-consortium111');

        $statusCode = $client->getResponse()->getStatusCode();
        $content = $client->getResponse()->getContent();

        $json = json_decode($content, true);

        $this->assertEquals(404, $statusCode);

        $this->assertEquals(true, strlen($content) > 0);
        $this->assertEquals(true, count($json) == 2);

        $this->assertEquals(true, array_key_exists('status', $json));
        $this->assertEquals('error', $json['status']);

        $message = $json['message'];
        $this->assertEquals(true, is_string($message));
        $this->assertEquals('Invalid response status code.', $message);
    }

    /**
     * Test multiple repository call to Github API with success responses
     */
    public function test20RepositoryFetch() {
        $client = static::createClient();

        $multiply = 20;
        $names = str_repeat("octocat,", $multiply);
        $names = substr($names, 0, strlen($names) - 1);

        $repos = str_repeat("git-consortium,", $multiply);
        $repos = substr($repos, 0, strlen($repos) - 1);

        $startTime = microtime(true);
        $client->request('GET', '/git/repositories/multi/' . $names . '/' . $repos);
        $elapsedTime = microtime(true) - $startTime;

        $this->assertEquals(true, $elapsedTime <= 1.0);
    }
}