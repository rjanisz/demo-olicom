###README

To run this project it's needed to do some steps:

##### 1
`git clone https://rjanisz@bitbucket.org/rjanisz/demo-olicom.git`

##### 2
`composer install`

##### 3
It's needed to provide Github access token in .env file.

##### 4
`php bin/phpunit`

##### 5
On production environment it's needed to set `APP_ENV=prod` in .env file.


Comments
- The github API has rate limiter
- The best and the easiest way would be to set up docker image.
- In async requests endpoints and code I should better validate input values
